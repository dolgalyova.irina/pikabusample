package com.dolgalyova.test.app.di

import androidx.fragment.app.FragmentActivity
import com.dolgalyova.test.BuildConfig
import com.dolgalyova.test.R
import com.dolgalyova.test.common.arch.navigation.ActivityNavigationHost
import com.dolgalyova.test.common.arch.navigation.NavigationHost
import com.dolgalyova.test.common.log.TimberRemoteTree
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.data.post.PostDataModule
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module(
    includes = [PostDataModule::class]
)
class AppModule {

    @Provides
    @Singleton
    fun retrofitBuilder(): Retrofit = Retrofit.Builder()
        .client(httpClient())
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .baseUrl(BuildConfig.API_URL)
        .build()

    @Provides
    @Singleton
    fun workers() = RxWorkers(
        subscribeWorker = Schedulers.io(),
        observeWorker = AndroidSchedulers.mainThread()
    )

    @Provides
    @Singleton
    fun timberTree(): Timber.Tree {
        return if (BuildConfig.DEBUG) Timber.DebugTree() else TimberRemoteTree()
    }

    @Provides
    @Singleton
    fun navigationHost(): NavigationHost<FragmentActivity> =
        ActivityNavigationHost(R.id.fragmentContainer)

    private fun httpClient(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) BODY else NONE
        })
        .build()
}