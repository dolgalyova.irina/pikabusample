package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.UIState
import com.dolgalyova.test.domain.model.Post

data class PostDetailsState(
    val post: Post?,
    val isLoading: Boolean
) : UIState {

    companion object {
        val EMPTY = PostDetailsState(
            post = null,
            isLoading = false
        )
    }
}