package com.dolgalyova.test.domain

import com.dolgalyova.test.data.post.PostRepository
import com.dolgalyova.test.data.post.model.PostDto
import com.dolgalyova.test.domain.model.Image
import com.dolgalyova.test.domain.model.Post
import io.reactivex.Single

class PostService(private val repository: PostRepository) {

    fun getPostList(): Single<List<Post>> {
        return repository.getList(page = 0)
            .map { posts -> posts.map { dto -> convertToPost(dto) } }
    }

    fun getPostDetails(postId: Int): Single<Post> {
        return repository.getById(postId)
            .map { dto ->
                Post(
                    id = dto.id,
                    date = dto.date,
                    title = dto.title,
                    text = dto.text,
                    likesCount = dto.likesCount,
                    images = dto.images.map { Image(url = it) }
                )
            }
    }

    private fun convertToPost(dto: PostDto): Post {
        return Post(
            id = dto.id,
            date = dto.date,
            title = dto.title,
            text = dto.text,
            likesCount = dto.likesCount,
            images = emptyList()
        )
    }


}