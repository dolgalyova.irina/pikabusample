package com.dolgalyova.test.data.post.source

import com.dolgalyova.test.common.arch.data.LocalDataSource
import com.dolgalyova.test.data.post.model.PostDetailsDto
import com.dolgalyova.test.data.post.model.PostDto

// Key == page number
typealias PostLocalSource = LocalDataSource<Int, List<PostDto>>

typealias PostDetailsLocalSource = LocalDataSource<Int, PostDetailsDto>