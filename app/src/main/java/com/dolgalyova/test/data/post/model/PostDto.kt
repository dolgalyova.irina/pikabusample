package com.dolgalyova.test.data.post.model

import java.util.*

data class PostDto(
    val id: Int,
    val date: Date?,
    val title: String,
    val text: String,
    val likesCount: Int
)