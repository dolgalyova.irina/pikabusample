package com.dolgalyova.test.screen.post_details

data class PostDetailsInputParams(val postId: Int)
