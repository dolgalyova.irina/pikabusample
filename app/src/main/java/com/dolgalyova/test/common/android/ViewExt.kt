package com.dolgalyova.test.common.android

import android.view.View
import androidx.annotation.StringRes

fun View.getString(@StringRes resId:Int) = resources.getString(resId)