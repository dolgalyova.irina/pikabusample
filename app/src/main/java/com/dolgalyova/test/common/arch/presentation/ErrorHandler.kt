package com.dolgalyova.test.common.arch.presentation

typealias ErrorHandler = (Throwable) -> Unit