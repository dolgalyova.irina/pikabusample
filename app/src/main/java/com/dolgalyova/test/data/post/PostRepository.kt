package com.dolgalyova.test.data.post

import com.dolgalyova.test.common.rx.toSingle
import com.dolgalyova.test.data.post.model.PostDetailsDto
import com.dolgalyova.test.data.post.model.PostDto
import com.dolgalyova.test.data.post.source.PostDetailsLocalSource
import com.dolgalyova.test.data.post.source.PostDetailsRemoteSource
import com.dolgalyova.test.data.post.source.PostLocalSource
import com.dolgalyova.test.data.post.source.PostRemoteSource
import io.reactivex.Single

class PostRepository(
    private val postRemoteSource: PostRemoteSource,
    private val postDetailsRemoteSource: PostDetailsRemoteSource,
    private val postLocalSource: PostLocalSource,
    private val postDetailsLocalSource: PostDetailsLocalSource
) {

    fun getById(id: Int): Single<PostDetailsDto> {
        return postDetailsLocalSource.getItem(id)
            .switchIfEmpty(postDetailsRemoteSource.getById(id)
                .flatMap {
                    postDetailsLocalSource.putItem(id, it).andThen(it.toSingle())
                })
    }

    fun getList(page: Int): Single<List<PostDto>> {
        return postLocalSource.getItem(page)
            .switchIfEmpty(postRemoteSource.getList()
                .flatMap {
                    postLocalSource.putItem(page, it).andThen(it.toSingle())
                }
            )
    }
}