package com.dolgalyova.test.data.post.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PostApi {

    @GET("posts.json")
    fun getPosts(): Single<RawPostListResponse>

    @GET("{id}.json")
    fun getPostDetails(@Path("id") id: Int): Single<RawPostDetailsResponse>
}