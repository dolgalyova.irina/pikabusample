package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.StateToModelMapper
import com.dolgalyova.test.common.utill.formatToString
import com.dolgalyova.test.domain.model.Post
import com.dolgalyova.test.screen.post_list.presentation.PostListSort.*
import com.dolgalyova.test.screen.post_list.view.PostListItem

class PostListStateModelMapper : StateToModelMapper<PostListState, PostListPresentationModel> {

    override fun mapStateToModel(state: PostListState): PostListPresentationModel {
        val uniqueItems = state.items.toSet()
        val sortedItems = when (state.sort) {
            RatingAsc -> uniqueItems.sortedBy { it.likesCount }
            RatingDesc -> uniqueItems.sortedByDescending { it.likesCount }
            DateAsc -> uniqueItems.sortedBy { it.date }
            DateDesc -> uniqueItems.sortedByDescending { it.date }
            None -> uniqueItems
        }
        val items = sortedItems.map { convertToListItem(it) }

        return PostListPresentationModel(
            isLoading = state.isLoading,
            sort = state.sort,
            items = items
        )
    }

    private fun convertToListItem(post: Post): PostListItem {
        return PostListItem(
            id = post.id,
            title = post.title,
            text = post.text,
            likesCount = post.likesCount.toString(),
            date = post.date?.formatToString().orEmpty()
        )
    }
}
