package com.dolgalyova.test.screen.post_list.view

data class PostListItem(
    val id: Int,
    val title: String,
    val text: String,
    val likesCount: String,
    val date: String
)