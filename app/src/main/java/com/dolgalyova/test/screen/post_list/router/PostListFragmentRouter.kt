package com.dolgalyova.test.screen.post_list.router

import com.dolgalyova.test.screen.container.navigation.Navigator
import com.dolgalyova.test.screen.container.navigation.Screen

class PostListFragmentRouter(private val navigator: Navigator) : PostListRouter {

    override fun openDetails(id: Int) {
        navigator.setScreen(Screen.PostDetails(id))
    }
}