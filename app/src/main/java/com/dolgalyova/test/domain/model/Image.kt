package com.dolgalyova.test.domain.model

inline class Image(val url: String)