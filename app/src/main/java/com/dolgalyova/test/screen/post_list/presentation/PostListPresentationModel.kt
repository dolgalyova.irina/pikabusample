package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.UIModel
import com.dolgalyova.test.screen.post_list.view.PostListItem

data class PostListPresentationModel(
    val isLoading: Boolean,
    val sort: PostListSort,
    val items: List<PostListItem>
) : UIModel