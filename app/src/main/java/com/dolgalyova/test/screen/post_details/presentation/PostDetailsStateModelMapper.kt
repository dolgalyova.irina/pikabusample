package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.StateToModelMapper
import com.dolgalyova.test.common.utill.formatToString

class PostDetailsStateModelMapper :
    StateToModelMapper<PostDetailsState, PostDetailsPresentationModel> {

    override fun mapStateToModel(state: PostDetailsState): PostDetailsPresentationModel {
        return PostDetailsPresentationModel(
            title = state.post?.title.orEmpty(),
            text = state.post?.text.orEmpty(),
            likesCount = (state.post?.likesCount ?: 0).toString(),
            date = state.post?.date?.formatToString().orEmpty(),
            images = state.post?.images.orEmpty().map { it.url },
            isLoading = state.isLoading
        )
    }
}