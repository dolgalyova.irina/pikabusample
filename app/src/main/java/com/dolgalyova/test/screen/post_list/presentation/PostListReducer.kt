package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.Reducer

class PostListReducer : Reducer<PostListState, PostListChange> {

    override fun reduce(state: PostListState, change: PostListChange): PostListState {
        return when (change) {
            is PostListChange.SortChanged -> state.copy(sort = change.sort)
            is PostListChange.LoadingChanged -> state.copy(isLoading = change.isLoading)
            is PostListChange.ItemsLoaded -> state.copy(items = state.items + change.items)
        }
    }
}