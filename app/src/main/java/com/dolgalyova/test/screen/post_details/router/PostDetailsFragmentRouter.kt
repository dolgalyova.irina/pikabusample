package com.dolgalyova.test.screen.post_details.router

import com.dolgalyova.test.screen.container.navigation.Navigator

class PostDetailsFragmentRouter(private val navigator: Navigator) : PostDetailsRouter {

    override fun close() = navigator.goBack()
}