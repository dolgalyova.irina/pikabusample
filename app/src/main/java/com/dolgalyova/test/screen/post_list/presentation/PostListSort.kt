package com.dolgalyova.test.screen.post_list.presentation

enum class PostListSort {
    RatingAsc, RatingDesc, DateAsc, DateDesc, None
}