package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.ErrorEvent
import com.dolgalyova.test.common.arch.presentation.ErrorHandler
import com.dolgalyova.test.common.arch.presentation.ReduxViewModel
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.common.rx.plusAssign
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.post_details.PostDetailsInputParams
import com.dolgalyova.test.screen.post_details.router.PostDetailsRouter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class PostDetailsViewModel(
    private val postService: PostService,
    private val inputParams: PostDetailsInputParams,
    private val router: PostDetailsRouter,
    reducer: PostDetailsReducer,
    stateModelMapper: PostDetailsStateModelMapper,
    workers: RxWorkers
) : ReduxViewModel<PostDetailsAction, PostDetailsChange, PostDetailsState, PostDetailsPresentationModel>(
    reducer = reducer,
    stateToModelMapper = stateModelMapper,
    workers = workers
) {
    private val changes = PublishSubject.create<PostDetailsChange>()
    override var state = PostDetailsState.EMPTY
    override val errorHandler: ErrorHandler = ::onError

    override fun onObserverActive(firstTime: Boolean) {
        super.onObserverActive(firstTime)
        if (firstTime) bindActions()
    }

    override fun provideChangesObservable(): Observable<PostDetailsChange> {
        val postChange = postService.getPostDetails(inputParams.postId)
            .map { PostDetailsChange.DataLoaded(it) }
            .doOnSubscribe { changes.onNext(PostDetailsChange.LoadingChanged(isLoading = true)) }
            .doOnTerminate { changes.onNext(PostDetailsChange.LoadingChanged(isLoading = false)) }
            .toObservable()
        return changes
            .mergeWith(postChange)
    }

    private fun bindActions() {
        disposables += actions.subscribe({
            when (it) {
                PostDetailsAction.CloseClick -> router.close()
            }
        }, ::onError)
    }

    private fun onError(throwable: Throwable) {
        event.value = ErrorEvent.SomethingWrongEvent
    }
}