package com.dolgalyova.test.common.android

interface OnBackPressListener {
    fun onBackPress(): Boolean
}