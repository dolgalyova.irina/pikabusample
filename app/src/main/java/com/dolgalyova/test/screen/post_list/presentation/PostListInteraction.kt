package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.UIAction
import com.dolgalyova.test.common.arch.presentation.UIEvent
import com.dolgalyova.test.common.arch.presentation.UIStateChange
import com.dolgalyova.test.domain.model.Post

sealed class PostListChange : UIStateChange {
    data class SortChanged(val sort: PostListSort) : PostListChange()
    data class LoadingChanged(val isLoading: Boolean) : PostListChange()
    data class ItemsLoaded(val items: List<Post>) : PostListChange()
}

sealed class PostListAction : UIAction {
    data class OpenPost(val id: Int) : PostListAction()
    object OnSortRatingAscClick : PostListAction()
    object OnSortRatingDescClick : PostListAction()
    object OnSortDateAscClick : PostListAction()
    object OnSortDateDescClick : PostListAction()
    object OnSortDefaultClick : PostListAction()
}

sealed class PostListEvent : UIEvent {
    object ScrollToTop : PostListEvent()
}