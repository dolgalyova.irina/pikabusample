package com.dolgalyova.test.screen.container.router

import com.dolgalyova.test.screen.container.navigation.Navigator
import com.dolgalyova.test.screen.container.navigation.Screen

class MainActivityRouter(private val navigator: Navigator) : MainRouter {
    override fun openPostList() = navigator.setScreen(Screen.PostList())

    override fun openPostDetails(postId: Int) = navigator.setScreen(Screen.PostDetails(postId))
}