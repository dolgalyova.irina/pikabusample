package com.dolgalyova.test.screen.post_details.view

import androidx.recyclerview.widget.DiffUtil

class ImageDiffCallback : DiffUtil.ItemCallback<String>() {

    override fun areItemsTheSame(oldItem: String, newItem: String) = oldItem == newItem

    override fun areContentsTheSame(oldItem: String, newItem: String) = oldItem == newItem
}