package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.Reducer

class PostDetailsReducer : Reducer<PostDetailsState, PostDetailsChange> {

    override fun reduce(state: PostDetailsState, change: PostDetailsChange): PostDetailsState {
        return when (change) {
            is PostDetailsChange.DataLoaded -> state.copy(post = change.post)
            is PostDetailsChange.LoadingChanged -> state.copy(isLoading = change.isLoading)
        }
    }
}