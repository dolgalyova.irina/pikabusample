package com.dolgalyova.test.data.post

import com.dolgalyova.test.data.post.api.PostApi
import com.dolgalyova.test.data.post.source.PostDetailsLocalSource
import com.dolgalyova.test.data.post.source.PostDetailsRemoteSource
import com.dolgalyova.test.data.post.source.PostLocalSource
import com.dolgalyova.test.data.post.source.PostRemoteSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class PostDataModule {

    @Provides
    @Singleton
    fun postRepository(
        postLocalSource: PostLocalSource,
        postRemoteSource: PostRemoteSource,
        postDetailsLocalSource: PostDetailsLocalSource,
        postDetailsRemoteSource: PostDetailsRemoteSource
    ): PostRepository = PostRepository(
        postRemoteSource = postRemoteSource,
        postDetailsRemoteSource = postDetailsRemoteSource,
        postLocalSource = postLocalSource,
        postDetailsLocalSource = postDetailsLocalSource
    )

    @Provides
    @Singleton
    fun postLocalSource() = PostLocalSource()

    @Provides
    @Singleton
    fun postDetailsLocalSource() = PostDetailsLocalSource()

    @Provides
    @Singleton
    fun postRemoteSource(api: PostApi) = PostRemoteSource(api = api)

    @Provides
    @Singleton
    fun postDetailsRemoteSource(api: PostApi) = PostDetailsRemoteSource(api = api)

    @Provides
    @Singleton
    fun postApi(retrofit: Retrofit): PostApi = retrofit.create(PostApi::class.java)
}