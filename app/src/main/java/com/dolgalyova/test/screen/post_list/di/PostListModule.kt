package com.dolgalyova.test.screen.post_list.di

import com.dolgalyova.test.common.di.ScreenScope
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.container.navigation.Navigator
import com.dolgalyova.test.screen.post_list.presentation.PostListViewModelFactory
import com.dolgalyova.test.screen.post_list.router.PostListFragmentRouter
import com.dolgalyova.test.screen.post_list.router.PostListRouter
import dagger.Module
import dagger.Provides

@Module
class PostListModule {

    @Provides
    @ScreenScope
    fun viewModelFactory(
        postService: PostService,
        router: PostListRouter,
        workers: RxWorkers
    ) = PostListViewModelFactory(
        postService = postService,
        router= router,
        workers = workers
    )

    @Provides
    @ScreenScope
    fun router(navigator: Navigator): PostListRouter = PostListFragmentRouter(navigator)
}