package com.dolgalyova.test.data.post.api

data class RawPost(
    val postId: Int,
    val timeshamp: Long,
    val title: String,
    val preview_text: String,
    val likes_count: Int
)