package com.dolgalyova.test.data.post.model

import java.util.*

data class PostDetailsDto(
    val id: Int,
    val date: Date,
    val title: String,
    val text: String,
    val images: List<String>,
    val likesCount: Int
)