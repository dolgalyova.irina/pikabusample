package com.dolgalyova.test.data.post.source

import com.dolgalyova.test.data.post.api.PostApi
import com.dolgalyova.test.data.post.api.RawPostDetails
import com.dolgalyova.test.data.post.model.PostDetailsDto
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit

class PostDetailsRemoteSource(private val api: PostApi) {

    fun getById(id: Int): Single<PostDetailsDto> {
        return api.getPostDetails(id)
            .map { convertToDto(it.post) }
    }

    private fun convertToDto(raw: RawPostDetails): PostDetailsDto {
        return PostDetailsDto(
            id = raw.postId,
            title = raw.title,
            text = raw.text,
            date = Date(TimeUnit.SECONDS.toMillis(raw.timeshamp)),
            images = raw.images,
            likesCount = raw.likes_count
        )
    }
}