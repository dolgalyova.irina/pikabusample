package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.ErrorEvent
import com.dolgalyova.test.common.arch.presentation.ErrorHandler
import com.dolgalyova.test.common.arch.presentation.ReduxViewModel
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.common.rx.plusAssign
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.post_list.router.PostListRouter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class PostListViewModel(
    private val postService: PostService,
    private val router: PostListRouter,
    reducer: PostListReducer,
    stateModelMapper: PostListStateModelMapper,
    workers: RxWorkers
) : ReduxViewModel<PostListAction, PostListChange, PostListState, PostListPresentationModel>(
    reducer = reducer,
    stateToModelMapper = stateModelMapper,
    workers = workers
) {
    private val changesSubject = PublishSubject.create<PostListChange>()
    override var state = PostListState.EMPTY
    override val errorHandler: ErrorHandler = ::onError

    override fun onObserverActive(firstTime: Boolean) {
        super.onObserverActive(firstTime)
        if (firstTime) bindActions()
    }

    override fun provideChangesObservable(): Observable<PostListChange> {
        val postsChange = postService.getPostList()
            .doOnSubscribe { changesSubject.onNext(PostListChange.LoadingChanged(isLoading = true)) }
            .doOnTerminate { changesSubject.onNext(PostListChange.LoadingChanged(isLoading = false)) }
            .map { PostListChange.ItemsLoaded(it) }
            .toObservable()

        return changesSubject
            .mergeWith(postsChange)
    }

    private fun bindActions() {
        disposables += actions.subscribe({
            when (it) {
                is PostListAction.OpenPost -> router.openDetails(it.id)
                PostListAction.OnSortRatingAscClick -> changeSort(PostListSort.RatingAsc)
                PostListAction.OnSortRatingDescClick -> changeSort(PostListSort.RatingDesc)
                PostListAction.OnSortDateAscClick -> changeSort(PostListSort.DateAsc)
                PostListAction.OnSortDateDescClick -> changeSort(PostListSort.DateDesc)
                PostListAction.OnSortDefaultClick -> changeSort(PostListSort.None)
            }
        }, ::onError)
    }

    private fun onError(error: Throwable) {
        event.value = ErrorEvent.ErrorMessageEvent(error.message.toString())
    }

    private fun changeSort(sort: PostListSort) {
        changesSubject.onNext(PostListChange.SortChanged(sort))
        event.value = PostListEvent.ScrollToTop

    }
}