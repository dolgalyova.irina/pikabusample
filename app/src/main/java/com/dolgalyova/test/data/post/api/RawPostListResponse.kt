package com.dolgalyova.test.data.post.api

data class RawPostListResponse(val posts: List<RawPost>)