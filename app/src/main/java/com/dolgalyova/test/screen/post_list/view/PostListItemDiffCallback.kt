package com.dolgalyova.test.screen.post_list.view

import androidx.recyclerview.widget.DiffUtil


class PostListItemDiffCallback : DiffUtil.ItemCallback<PostListItem>() {

    override fun areItemsTheSame(oldItem: PostListItem, newItem: PostListItem) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PostListItem, newItem: PostListItem) = oldItem == newItem
}