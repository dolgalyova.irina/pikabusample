package com.dolgalyova.test.screen.post_details.di

import com.dolgalyova.test.common.di.ScreenScope
import com.dolgalyova.test.screen.post_details.PostDetailsFragment
import com.dolgalyova.test.screen.post_details.PostDetailsInputParams
import dagger.BindsInstance
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [PostDetailsModule::class])
interface PostDetailsComponent {

    fun inject(target: PostDetailsFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance inputParams: PostDetailsInputParams): PostDetailsComponent
    }

    interface ComponentProvider {
        fun providePostDetailsComponent(inputParams: PostDetailsInputParams): PostDetailsComponent
    }
}