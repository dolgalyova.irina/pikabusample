package com.dolgalyova.test.screen.container.di

import androidx.fragment.app.FragmentActivity
import com.dolgalyova.test.common.arch.navigation.NavigationHost
import com.dolgalyova.test.common.di.ActivityScope
import com.dolgalyova.test.screen.container.MainViewModelFactory
import com.dolgalyova.test.screen.container.navigation.Navigator
import com.dolgalyova.test.screen.container.router.MainActivityRouter
import com.dolgalyova.test.screen.container.router.MainRouter
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun navigator(host: NavigationHost<FragmentActivity>) = Navigator(
        host = host
    )

    @Provides
    @ActivityScope
    fun router(navigator: Navigator): MainRouter = MainActivityRouter(
        navigator = navigator
    )

    @Provides
    @ActivityScope
    fun mainViewModelFactory(router: MainRouter) = MainViewModelFactory(
        router = router
    )
}