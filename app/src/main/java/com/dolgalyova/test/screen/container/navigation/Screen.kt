package com.dolgalyova.test.screen.container.navigation

import androidx.fragment.app.Fragment
import com.dolgalyova.test.screen.post_details.PostDetailsFragment
import com.dolgalyova.test.screen.post_list.PostListFragment

sealed class Screen {
    abstract val addToBackStack: Boolean
    abstract fun createFragment(): Fragment

    class PostList : Screen() {
        override val addToBackStack = false
        override fun createFragment() = PostListFragment.create()
    }

    class PostDetails(private val postId: Int) : Screen() {
        override val addToBackStack = true
        override fun createFragment() = PostDetailsFragment.create(postId = postId)
    }
}