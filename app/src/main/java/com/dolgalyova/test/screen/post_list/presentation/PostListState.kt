package com.dolgalyova.test.screen.post_list.presentation

import com.dolgalyova.test.common.arch.presentation.UIState
import com.dolgalyova.test.domain.model.Post
import com.dolgalyova.test.screen.post_list.presentation.PostListSort.None

data class PostListState(
    val items: List<Post>,
    val isLoading: Boolean,
    val sort: PostListSort
) : UIState {
    companion object {
        val EMPTY = PostListState(
            items = emptyList(),
            isLoading = false,
            sort = None
        )
    }
}
