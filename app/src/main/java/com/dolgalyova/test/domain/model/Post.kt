package com.dolgalyova.test.domain.model

import java.util.*

data class Post(
    val id: Int,
    val date: Date?,
    val title: String,
    val text: String,
    val likesCount: Int,
    val images: List<Image>
)