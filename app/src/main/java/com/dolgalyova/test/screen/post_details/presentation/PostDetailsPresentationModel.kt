package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.UIModel

data class PostDetailsPresentationModel(
    val title: String,
    val text: String,
    val likesCount: String,
    val date: String,
    val images: List<String>,
    val isLoading:Boolean
) : UIModel