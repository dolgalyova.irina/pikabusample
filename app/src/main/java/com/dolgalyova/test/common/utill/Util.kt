package com.dolgalyova.test.common.utill

inline fun <T> getOrNull(body: () -> T): T? = try {
    body()
} catch (e: Exception) {
    null
}