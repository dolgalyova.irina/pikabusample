package com.dolgalyova.test.screen.container.di

import com.dolgalyova.test.common.di.ActivityScope
import com.dolgalyova.test.domain.EntityServiceModule
import com.dolgalyova.test.screen.container.MainActivity
import com.dolgalyova.test.screen.post_details.di.PostDetailsComponent
import com.dolgalyova.test.screen.post_list.di.PostListComponent
import dagger.BindsInstance
import dagger.Subcomponent

@ActivityScope
@Subcomponent(
    modules = [
        EntityServiceModule::class,
        MainModule::class]
)
interface MainComponent {

    fun inject(target: MainActivity)

    fun plusPostList(): PostListComponent.Factory

    fun plusPostDetails(): PostDetailsComponent.Factory

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance target: MainActivity): MainComponent
    }

    interface ComponentProvider {
        fun provideMainComponent(activity: MainActivity): MainComponent
    }
}