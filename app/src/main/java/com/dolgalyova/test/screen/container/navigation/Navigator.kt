package com.dolgalyova.test.screen.container.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.dolgalyova.test.common.arch.navigation.NavigationHost

class Navigator(private val host: NavigationHost<*>) {

    fun setScreen(screen: Screen) {
        host.doWhenAvailable { fragmentManager: FragmentManager, containerId: Int ->
            val fragment = screen.createFragment()
            setFragment(fragmentManager, containerId, fragment, screen.addToBackStack)
        }
    }

    fun goBack() {
        host.doWhenAvailable { fragmentManager, _ -> fragmentManager.popBackStack() }
    }

    private fun setFragment(
        fm: FragmentManager,
        containerId: Int,
        fragment: Fragment,
        addToBackStack: Boolean
    ) {
        val transaction = fm.beginTransaction()
            .replace(containerId, fragment, fragment::class.java.name)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)

        if (addToBackStack) transaction.addToBackStack(null)

        transaction.commit()
    }
}