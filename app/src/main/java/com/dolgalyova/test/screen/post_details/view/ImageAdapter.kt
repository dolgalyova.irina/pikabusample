package com.dolgalyova.test.screen.post_details.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.test.R
import com.dolgalyova.test.app.GlideApp
import kotlinx.android.synthetic.main.item_image.view.*

class ImageAdapter : ListAdapter<String, ImageAdapter.ImageViewHolder>(ImageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_image, parent, false)
        return ImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        GlideApp.with(holder.itemView)
            .load(getItem(position))
            .into(holder.itemView.image)
    }

    inner class ImageViewHolder(v: View) : RecyclerView.ViewHolder(v)
}