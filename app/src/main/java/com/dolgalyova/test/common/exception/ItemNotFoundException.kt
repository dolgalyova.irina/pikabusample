package com.dolgalyova.test.common.exception

class ItemNotFoundException(id: String) :
    IllegalArgumentException("Item with id $id can not be found")