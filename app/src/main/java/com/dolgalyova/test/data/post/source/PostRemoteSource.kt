package com.dolgalyova.test.data.post.source

import com.dolgalyova.test.data.post.api.PostApi
import com.dolgalyova.test.data.post.api.RawPost
import com.dolgalyova.test.data.post.model.PostDto
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit

class PostRemoteSource(private val api: PostApi) {

    fun getList(): Single<List<PostDto>> {
        return api.getPosts()
            .map { it.posts }
            .flatMapObservable { Observable.fromIterable(it) }
            .map { convertToDto(it) }
            .toList()
    }

    private fun convertToDto(raw: RawPost) = PostDto(
        id = raw.postId,
        title = raw.title,
        text = raw.preview_text,
        likesCount = raw.likes_count,
        date = Date(TimeUnit.SECONDS.toMillis(raw.timeshamp))
    )
}