package com.dolgalyova.test.screen.post_list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.test.R
import kotlinx.android.synthetic.main.item_post.view.*

class PostListAdapter(private val onItemClick: (PostListItem) -> Unit = {}) :
    ListAdapter<PostListItem, RecyclerView.ViewHolder>(PostListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_post, parent, false)
        return PostViewHolder(view, onItemClick)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        bindPost(holder as PostViewHolder, item, position)
    }

    private fun bindPost(holder: PostViewHolder, item: PostListItem, position: Int) =
        with(holder.itemView) {
            holder.item = item
            title.text = item.title
            date.text = item.date
            text.text = HtmlCompat.fromHtml(item.text, HtmlCompat.FROM_HTML_MODE_COMPACT)
            likesCount.text = item.likesCount
        }

    inner class PostViewHolder(v: View, onItemClick: (PostListItem) -> Unit) :
        RecyclerView.ViewHolder(v) {
        internal var item: PostListItem? = null

        init {
            itemView.setOnClickListener { item?.run(onItemClick) }
        }
    }
}
