package com.dolgalyova.test.common.android

import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.dolgalyova.test.R
import com.google.android.material.snackbar.Snackbar

fun Fragment.showGeneralError(text: String? = null) {
    view?.let {
        Snackbar.make(it, text ?: getString(R.string.error_general), Snackbar.LENGTH_LONG).apply {
            view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorError))
            show()
        }
    }
}