package com.dolgalyova.test.common.utill

import java.text.SimpleDateFormat
import java.util.*

fun Date.formatToString(): String {
    return getOrNull { SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(this) }.orEmpty()
}

fun Date.formatShortDate(): String {
    return getOrNull { SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(this) }.orEmpty()
}