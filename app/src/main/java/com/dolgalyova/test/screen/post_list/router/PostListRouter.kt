package com.dolgalyova.test.screen.post_list.router

interface PostListRouter {
    fun openDetails(id: Int)
}