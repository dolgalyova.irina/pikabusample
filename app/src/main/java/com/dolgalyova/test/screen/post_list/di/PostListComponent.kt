package com.dolgalyova.test.screen.post_list.di

import com.dolgalyova.test.common.di.ScreenScope
import com.dolgalyova.test.screen.post_list.PostListFragment
import dagger.Subcomponent


@ScreenScope
@Subcomponent(modules = [PostListModule::class])
interface PostListComponent {

    fun inject(target: PostListFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(): PostListComponent
    }

    interface ComponentProvider {
        fun providePostListComponent(): PostListComponent
    }
}