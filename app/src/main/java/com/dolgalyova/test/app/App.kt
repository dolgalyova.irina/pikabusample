package com.dolgalyova.test.app

import android.app.Application
import com.dolgalyova.test.app.di.DaggerAppComponent
import com.dolgalyova.test.screen.container.MainActivity
import com.dolgalyova.test.screen.container.di.MainComponent
import timber.log.Timber
import javax.inject.Inject

class App : Application(), MainComponent.ComponentProvider {

    private val component by lazy {
        DaggerAppComponent.factory().create(this)
    }

    @Inject lateinit var logTree: Timber.Tree

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        Timber.plant(logTree)
    }

    override fun provideMainComponent(activity: MainActivity): MainComponent =
        component.plusMain().create(activity)
}