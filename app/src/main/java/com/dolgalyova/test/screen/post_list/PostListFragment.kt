package com.dolgalyova.test.screen.post_list

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.test.R
import com.dolgalyova.test.common.android.RecyclerViewAdapterObserver
import com.dolgalyova.test.common.android.showGeneralError
import com.dolgalyova.test.common.arch.presentation.ErrorEvent
import com.dolgalyova.test.common.arch.presentation.UIEvent
import com.dolgalyova.test.screen.post_list.di.PostListComponent
import com.dolgalyova.test.screen.post_list.presentation.*
import com.dolgalyova.test.screen.post_list.view.PostListAdapter
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject

class PostListFragment : Fragment(R.layout.fragment_post_list) {

    companion object {
        fun create() = PostListFragment()
    }

    private val component by lazy {
        (activity as PostListComponent.ComponentProvider).providePostListComponent()
    }
    private val viewModel by lazy {
        ViewModelProvider(this, viewModeFactory).get(PostListViewModel::class.java)
    }
    @Inject lateinit var viewModeFactory: PostListViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.observableModel.observe(viewLifecycleOwner, Observer { renderModel(it) })
        viewModel.observableEvent.observe(viewLifecycleOwner, Observer { renderEvent(it) })
    }

    private fun renderModel(model: PostListPresentationModel) {
        progress.isVisible = model.isLoading
        (postList.adapter as PostListAdapter).submitList(model.items)
    }

    private fun renderEvent(event: UIEvent) {
        when (event) {
            PostListEvent.ScrollToTop -> handleScrollToTop()
            ErrorEvent.SomethingWrongEvent -> {
                progress.isVisible = false
                showGeneralError()
            }
            is ErrorEvent.ErrorMessageEvent -> {
                showGeneralError(text = event.message)
            }
        }
    }

    private fun initViews() {
        toolbar.overflowIcon = ContextCompat.getDrawable(toolbar.context, R.drawable.ic_sort)
        toolbar.inflateMenu(R.menu.menu_post_list)
        toolbar.setOnMenuItemClickListener {
            val action = when (it.itemId) {
                R.id.sortRatingAsc -> PostListAction.OnSortRatingAscClick
                R.id.sortRatingDesc -> PostListAction.OnSortRatingDescClick
                R.id.sortDateAsc -> PostListAction.OnSortDateAscClick
                R.id.sortDateDesc -> PostListAction.OnSortDateDescClick
                R.id.sortNone -> PostListAction.OnSortDefaultClick
                else -> PostListAction.OnSortDefaultClick
            }
            viewModel.dispatch(action)
            true
        }
        with(postList) {
            layoutManager = LinearLayoutManager(context)
                .apply { orientation = RecyclerView.VERTICAL }
            adapter = PostListAdapter(onItemClick = {
                viewModel.dispatch(PostListAction.OpenPost(it.id))
            })
        }
    }

    private fun handleScrollToTop() {
        postList?.scrollToPosition(0)
        postList.adapter?.registerAdapterDataObserver(RecyclerViewAdapterObserver(onChange = {
            postList?.scrollToPosition(0)
        }))
    }
}