package com.dolgalyova.test.domain

import com.dolgalyova.test.data.post.PostRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class EntityServiceModule {

    @Provides
    @Reusable
    fun postService(postRepository: PostRepository) = PostService(postRepository)
}