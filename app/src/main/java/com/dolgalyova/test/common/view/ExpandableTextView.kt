package com.dolgalyova.test.common.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import com.dolgalyova.test.R
import com.dolgalyova.test.common.android.getString
import kotlinx.android.synthetic.main.layout_expandable_text_view.view.*

private const val DEFAULT_MAX_LINES = 2

class ExpandableTextView : LinearLayoutCompat {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    var maxLines = DEFAULT_MAX_LINES
    var isExpanded: Boolean
        get() = textView.maxLines == Integer.MAX_VALUE
        set(value) {
            textView.maxLines = if (value) Integer.MAX_VALUE else maxLines
            readMore.text = getString(
                if (isExpanded) R.string.label_read_less else R.string.label_read_more
            )
        }
    var text: CharSequence
        get() = textView.text
        set(value) {
            textView.text = value
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_expandable_text_view, this)
        textView.onLinesCountChanged = ::onLinesCountChanged
        readMore.setOnClickListener { onToggleLineCountClick() }
        isExpanded = false
    }

    private fun onLinesCountChanged(count: Int) {
        readMore.isVisible = count > maxLines
        isExpanded = !readMore.isVisible
    }

    private fun onToggleLineCountClick() {
        isExpanded = !isExpanded
    }
}