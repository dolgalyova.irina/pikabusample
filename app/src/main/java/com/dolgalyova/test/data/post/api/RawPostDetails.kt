package com.dolgalyova.test.data.post.api

data class RawPostDetails(
    val postId: Int,
    val timeshamp: Long,
    val title: String,
    val text: String,
    val images: List<String>,
    val likes_count: Int
)