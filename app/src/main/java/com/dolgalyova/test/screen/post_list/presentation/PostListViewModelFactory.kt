package com.dolgalyova.test.screen.post_list.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.post_list.router.PostListRouter

class PostListViewModelFactory(
    private val postService: PostService,
    private val router: PostListRouter,
    private val workers: RxWorkers

) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostListViewModel(
            postService = postService,
            router = router,
            reducer = PostListReducer(),
            stateModelMapper = PostListStateModelMapper(),
            workers = workers
        ) as T
    }
}