package com.dolgalyova.test.screen.post_details

import android.os.Bundle
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.dolgalyova.test.BuildConfig
import com.dolgalyova.test.R
import com.dolgalyova.test.common.android.OnBackPressListener
import com.dolgalyova.test.common.android.showGeneralError
import com.dolgalyova.test.common.arch.presentation.ErrorEvent
import com.dolgalyova.test.common.arch.presentation.UIEvent
import com.dolgalyova.test.screen.post_details.di.PostDetailsComponent
import com.dolgalyova.test.screen.post_details.presentation.PostDetailsAction
import com.dolgalyova.test.screen.post_details.presentation.PostDetailsPresentationModel
import com.dolgalyova.test.screen.post_details.presentation.PostDetailsViewModel
import com.dolgalyova.test.screen.post_details.presentation.PostDetailsViewModelFactory
import com.dolgalyova.test.screen.post_details.view.ImageAdapter
import kotlinx.android.synthetic.main.fragment_post_details.*
import javax.inject.Inject

class PostDetailsFragment : Fragment(R.layout.fragment_post_details), OnBackPressListener {

    companion object {
        private const val EXTRA_POST_ID = "${BuildConfig.APPLICATION_ID}.EXTRA_POST_ID"
        fun create(postId: Int): PostDetailsFragment {
            val args = Bundle().apply {
                this.putInt(EXTRA_POST_ID, postId)
            }
            return PostDetailsFragment().apply {
                arguments = args
            }
        }
    }

    private val component by lazy {
        val postId = arguments?.getInt(EXTRA_POST_ID) ?: -1
        val params = PostDetailsInputParams(postId)
        (activity as PostDetailsComponent.ComponentProvider).providePostDetailsComponent(params)
    }
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(PostDetailsViewModel::class.java)
    }

    @Inject lateinit var viewModelFactory: PostDetailsViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.observableModel.observe(viewLifecycleOwner, Observer { renderModel(it) })
        viewModel.observableEvent.observe(viewLifecycleOwner, Observer { renderEvent(it) })
    }

    override fun onBackPress(): Boolean {
        viewModel.dispatch(PostDetailsAction.CloseClick)
        return true
    }

    private fun renderModel(model: PostDetailsPresentationModel) {
        progress.isVisible = model.isLoading
        (images.adapter as ImageAdapter).submitList(model.images)
        toolbar.title = model.title
        title.text = model.title
        date.text = model.date
        text.text = HtmlCompat.fromHtml(model.text, HtmlCompat.FROM_HTML_MODE_COMPACT)
        likesCount.text = model.likesCount
        likesCount.isVisible = true
    }

    private fun renderEvent(event: UIEvent) {
        if (event is ErrorEvent.SomethingWrongEvent) {
            progress.isVisible = false
            likesCount.isVisible = false
            showGeneralError()
        }
    }

    private fun initViews() {
        with(images) {
            layoutManager = LinearLayoutManager(context)
                .apply { orientation = RecyclerView.VERTICAL }
            adapter = ImageAdapter()
        }
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { onBackPress() }
    }

}
