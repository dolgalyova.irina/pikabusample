package com.dolgalyova.test.screen.container

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.test.R
import com.dolgalyova.test.common.android.OnBackPressListener
import com.dolgalyova.test.common.arch.navigation.NavigationHost
import com.dolgalyova.test.screen.container.di.MainComponent
import com.dolgalyova.test.screen.post_details.PostDetailsInputParams
import com.dolgalyova.test.screen.post_details.di.PostDetailsComponent
import com.dolgalyova.test.screen.post_list.di.PostListComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity(R.layout.activity_main),
    PostListComponent.ComponentProvider, PostDetailsComponent.ComponentProvider {

    private val component by lazy {
        (application as MainComponent.ComponentProvider).provideMainComponent(this)
    }

    @Inject lateinit var navigationHost: NavigationHost<FragmentActivity>
    @Inject lateinit var viewModelFactory: MainViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
        navigationHost.attachHost(this)
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onBackPressed() {
        val topFragment = supportFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        val processed = if (topFragment?.isVisible == true) {
            (topFragment as OnBackPressListener).onBackPress()
        } else false
        if (!processed) super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        navigationHost.detachHost()
    }

    override fun providePostListComponent(): PostListComponent {
        return component.plusPostList().create()
    }

    override fun providePostDetailsComponent(inputParams: PostDetailsInputParams): PostDetailsComponent {
        return component.plusPostDetails().create(inputParams)
    }
}