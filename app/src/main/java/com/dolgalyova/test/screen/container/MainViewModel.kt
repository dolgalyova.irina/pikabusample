package com.dolgalyova.test.screen.container

import androidx.lifecycle.ViewModel
import com.dolgalyova.test.screen.container.router.MainRouter

class MainViewModel(router: MainRouter) : ViewModel() {

    init {
        router.openPostList()
    }
}