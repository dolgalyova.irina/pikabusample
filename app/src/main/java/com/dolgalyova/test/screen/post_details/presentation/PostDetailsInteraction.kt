package com.dolgalyova.test.screen.post_details.presentation

import com.dolgalyova.test.common.arch.presentation.UIAction
import com.dolgalyova.test.common.arch.presentation.UIEvent
import com.dolgalyova.test.common.arch.presentation.UIStateChange
import com.dolgalyova.test.domain.model.Post

sealed class PostDetailsChange : UIStateChange {
    data class DataLoaded(val post: Post) : PostDetailsChange()
    data class LoadingChanged(val isLoading: Boolean) : PostDetailsChange()
}

sealed class PostDetailsAction : UIAction {
    object CloseClick : PostDetailsAction()
}

sealed class PostDetailEvent : UIEvent