package com.dolgalyova.test.screen.post_details.di

import com.dolgalyova.test.common.di.ScreenScope
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.container.navigation.Navigator
import com.dolgalyova.test.screen.post_details.PostDetailsInputParams
import com.dolgalyova.test.screen.post_details.presentation.PostDetailsViewModelFactory
import com.dolgalyova.test.screen.post_details.router.PostDetailsFragmentRouter
import com.dolgalyova.test.screen.post_details.router.PostDetailsRouter
import dagger.Module
import dagger.Provides

@Module
class PostDetailsModule {

    @Provides
    @ScreenScope
    fun router(
        navigator: Navigator
    ): PostDetailsRouter = PostDetailsFragmentRouter(
        navigator = navigator
    )

    @Provides
    @ScreenScope
    fun viewModelFactory(
        router: PostDetailsRouter,
        postService: PostService,
        inputParams: PostDetailsInputParams,
        workers: RxWorkers
    ) = PostDetailsViewModelFactory(
        router = router,
        postService = postService,
        inputParams = inputParams,
        workers = workers
    )
}