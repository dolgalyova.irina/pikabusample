package com.dolgalyova.test.data.post.api

data class RawPostDetailsResponse(val post: RawPostDetails)