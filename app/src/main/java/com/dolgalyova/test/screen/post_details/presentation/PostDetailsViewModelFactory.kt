package com.dolgalyova.test.screen.post_details.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.test.common.rx.RxWorkers
import com.dolgalyova.test.domain.PostService
import com.dolgalyova.test.screen.post_details.PostDetailsInputParams
import com.dolgalyova.test.screen.post_details.router.PostDetailsRouter

class PostDetailsViewModelFactory(
    private val router: PostDetailsRouter,
    private val postService: PostService,
    private val inputParams: PostDetailsInputParams,
    private val workers: RxWorkers

) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostDetailsViewModel(
            router = router,
            postService = postService,
            inputParams = inputParams,
            reducer = PostDetailsReducer(),
            stateModelMapper = PostDetailsStateModelMapper(),
            workers = workers
        ) as T
    }
}