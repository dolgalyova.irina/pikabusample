package com.dolgalyova.test.screen.container.router

interface MainRouter {
    fun openPostDetails(postId: Int)
    fun openPostList()
}