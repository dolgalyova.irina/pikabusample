package com.dolgalyova.test.screen.container

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.test.screen.container.router.MainRouter

class MainViewModelFactory(private val router: MainRouter) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(
            router = router
        ) as T
    }
}