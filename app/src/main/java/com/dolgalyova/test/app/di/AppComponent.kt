package com.dolgalyova.test.app.di

import com.dolgalyova.test.app.App
import com.dolgalyova.test.screen.container.di.MainComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(target: App)

    fun plusMain(): MainComponent.Factory

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance target: App): AppComponent
    }
}